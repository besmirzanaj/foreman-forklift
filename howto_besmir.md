grab the version match from here:

    vagrant/config/versions.yaml

and define here:

    $ WANTED_FOREMAN_VERSION=
    $ WANTED_KATELLO_VERSION=

example for CentOS 7 and Foreman 1.24

    export WANTED_FOREMAN_VERSION="1.24"
    export WANTED_KATELLO_VERSION="3.14"

Create an inventory file with your foreman server at inventories folder:

    cat ./inventories/foreman-cla
    [foreman]
    foreman.cloudalbania.com

Before running the playbook make sure to set the LANG on the ansible runner host to UTF-8

    export LANG="en_US.UTF-8"

Finally run the setup command:

    ansible-playbook -i inventories/foreman-cla -l foreman.cloudalbania.com playbooks/katello.yml -e foreman_repositories_version=$WANTED_FOREMAN_VERSION -e katello_repositories_version=$WANTED_KATELLO_VERSION

After installing, install python-gobject package on the Foreman server to be able
to sync centos 8 repos

    yum install python-gobject
